import Vue from 'vue'
import Vuex from 'vuex'
import VueI18n from 'vue-i18n'
import 'quasar/dist/quasar.umd.min.js'
import 'quasar/dist/quasar.min.css'
import 'material-icons/iconfont/material-icons.css'
import '@fortawesome/fontawesome-free/css/all.css'

import App from './App.vue'
import Store from './store'
import router from './router'
import translations from './translations'
import { ENV, PRODUCTION } from './constants'

Vue.config.productionTip = false

Vue.use(Vuex)
Vue.use(VueI18n)

const store = new Vuex.Store(Store)

const i18n = new VueI18n({
  locale: 'en', // we pick this in a smarter way in LanguagePicker.vue
  fallbackLocale: 'en',
  messages: translations
})

new Vue({
  store,
  i18n,
  router: router(store),
  render: (h) => h(App)
}).$mount('#app')

if (ENV !== PRODUCTION) {
  document.title = ENV.toUpperCase()
}
