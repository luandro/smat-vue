import Vue from 'vue'
import VueRouter from 'vue-router'
import AboutView from '@/views/AboutView'
import TimelineView from '@/views/TimelineView'
import HashtagsView from '@/views/HashtagsView'
import LinksView from '@/views/LinksView'
import ActivityView from '@/views/ActivityView'
import SettingsTimeline from '@/components/sidebar/SettingsTimeline'
import SettingsHashtags from '@/components/sidebar/SettingsHashtags'
import SettingsLinks from '@/components/sidebar/SettingsLinks'
import SettingsActivity from '@/components/sidebar/SettingsActivity'

export default function router (store) {
  Vue.use(VueRouter)

  const routes = [
    {
      path: '/',
      name: 'home',
      redirect: () => {
        const history = store.state.searchSettings.history

        return { name: history.lastUsedTool }
      }
    },
    {
      path: '/about',
      name: 'about',
      components: {
        page: AboutView
      }
    },
    {
      path: '/timeline',
      name: 'timeline',
      components: {
        page: TimelineView,
        sidebar: SettingsTimeline
      }
    },
    {
      path: '/hashtags',
      name: 'hashtags',
      components: {
        page: HashtagsView,
        sidebar: SettingsHashtags
      }
    },
    {
      path: '/links',
      name: 'links',
      components: {
        page: LinksView,
        sidebar: SettingsLinks
      }
    },
    {
      path: '/activity',
      name: 'activity',
      components: {
        page: ActivityView,
        sidebar: SettingsActivity
      }
    }
  ]

  return new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
  })
}
