import searchSettings from './modules/searchSettings'

export default {
  modules: {
    searchSettings
  }
}
