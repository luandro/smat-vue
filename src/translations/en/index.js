module.exports = {
  nav: {
    home: 'Home',
    about: 'About',
    timeline: 'Timeline',
    hashtags: 'Hashtags',
    links: 'Link Counter',
    activity: 'Activity'
  },
  toolPicker: {
    label: 'SMAT tool',
    changeTools: 'Change tools',
    modalTitle: 'Choose a tool to begin your analysis',
    modalSubtitle:
      'Each tool below will run a different analysis based on your input and return downloadable results'
  },
  toolDescription: {
    timeline:
      'The timeline functionality takes a search term and makes a graph of how often it was written over time',
    hashtags:
      'The hashtag functionality takes a search term and creates a bar graph of related hashtags',
    links:
      'The link counter functionality takes a search term and returns a bar graph counting links in comments with that term',
    activity:
      'This takes a search term and creates a bar graph of which authors or subreddits have the most activity'
  },
  setting: {
    term: 'Search term',
    termPlaceholder: 'Enter search term',
    startDate: 'Start date',
    endDate: 'End date',
    pickWebsite: 'Social media website to search',
    aggBy: {
      label: 'Aggregate Reddit by',
      author: 'Author',
      subreddit: 'Subreddit'
    },
    numberHashtags: 'Number of hashtags',
    numberUrls: 'Number of urls',
    interval: 'Interval',
    times: {
      hour: 'hour',
      day: 'day',
      week: 'week',
      month: 'month'
    },
    timely: {
      hour: 'Hourly',
      day: 'Daily',
      week: 'Weekly',
      month: 'Monthly'
    },
    limit: 'Limit',
    showChangepoint: 'Show changepoint',
    button: {
      timeline: 'See timeline results',
      hashtags: 'See hashtag detector results',
      urls: 'See URLs graph',
      activity: 'See activity'
    }
  },
  chart: {
    timelineOn: 'Timeline on ',
    hashtagsWith: 'Hashtags Occurring with ',
    linkCountOn: 'Link Count on ',
    activityOn: 'Activity on ',
    postsPer: "Posts containing '{term}' per {interval}",
    popularLinksWith: "Popular links occurring with '{term}'",
    termAggBy: "'{term}' aggregated over {aggBy}s",
    downloadPNG: 'Save as PNG'
  },
  table: {
    date: 'Date',
    usage: 'Usage',
    urls: 'URLs',
    urlCount: 'URL count',
    user: 'User',
    count: 'Count',
    hashtags: 'Hashtags',
    hashtagFreq: 'Hashtag frequency',
    viewFor: 'Table View for ',
    view: 'Table View',
    downloadCSV: 'Download CSV'
  },
  data: {
    beforeChangepoint: 'Before {site} changepoint',
    afterChangepoint: 'After {site} changepoint'
  },
  about: require('./about.js')
}
