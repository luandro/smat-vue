module.exports = {
  title: 'Sobre SMAT',
  subtitle: 'Fácil comprensión de las conversaciones en línea.',
  intro: 'El Kit de herramientas para análisis de redes sociales (SMAT) fue diseñado para apoyar a personas activistas, periodistas, investigadoras y organizaciones para analizar y visualizar tendencias amplias en diferentes plataformas.',
  // Buscamos ofrecer herramientas fáciles de usar que puedan ayudar a desarrollar narrativas sobre lo que está ocurriendo a gran escala en los entornos digitales. Las herramientas han sido diseñadas para ser eficientes y simples, para que quienes las utilicen puedan responder rápidamente a la desinformación y las campañas de odio mientras se propagan, promoviendo que el desarrollo de <a href="http://workshop-proceedings.icwsm.org/abstract?id=2020_18">investigaciones académicas</a> sobre el tema. Aunque hemos construido un marco de referencia para el uso de estas herramientas, las historias que están por descubrirse dependen de la curiosidad y disposición de quienes las utilicen.'
  value1: {
    title: 'Que tenemos',
    detail: 'El Kit de herramientas para análisis de redes sociales (SMAT) fue diseñado para apoyar a personas activistas, periodistas, investigadoras y organizaciones para analizar y visualizar tendencias amplias en diferentes plataformas. '
  },
  value2: {
    title: 'Sencilla y rigurosa',
    detail: 'Las herramientas han sido diseñadas para ser eficientes y simples, para que quienes las utilicen puedan responder rápidamente a la desinformación y las campañas de odio mientras se propagan, promoviendo que el desarrollo de investigaciones académicas sobre el tema.'
  },
  value3: {
    title: 'Open-source',
    detail: 'Nuestras herramientas son de open-source para que pueda ver cómo estamos trabajando y adaptarlas a sus necesidades.'
  },
  contact: {
    title: 'Salúdanos',
    detail: 'Encuéntranos en <a href="https://gitlab.com/smat-project/">gitlab</a> o salúdanos en rebelliousdata[at]gmail.com or @emmibevensee en Twitter. SMAT está alojado fiscalmente y las contribuciones se pueden realizar a través de nuestra <a href="https://opencollective.com/smat"> página </a> de Open Collective.'
  },

  contributors: {
    title: 'En este proyecto han colaborado',
    detail: 'Emmi Bevensee, Jason Baumgartner/Pushshift, Quinn Dougherty, Jeremy Blackburn, Harry Momand, ZC, Max Aliapoulios, <a href="https://jacob-karlsson.com/">Jacob Karlsson</a>, Alex K., <a href="http://protozoa.nz/">Protozoa Co-op</a>, Zach!, y más personas. Herramientas creadas con <a href="https://pushshift.io/">Pushshift</a>. Inspiradas por <a href="https://pushshift.io/slack-install/">slackbot.</a>. Diseños de <a href="https://holamimi.com/">Mek Frinchaboy</a>.'
  },
  partners: {
    // title: 'Our Partners'
  }
}
