module.exports = {
  nav: {
    home: 'Inicio',
    about: 'Acerca',
    timeline: 'Línea del tiempo',
    hashtags: 'Hashtags',
    links: 'Contador de enlaces',
    activity: 'Actividad'
  },
  setting: {
    term: 'Término de búsqueda',
    termPlaceholder: 'Ingrese el término de búsqueda',
    startDate: 'Fecha de inicio',
    endDate: 'Fecha final',
    pickWebsite: 'Sitio web de redes sociales para buscar',
    numberHashtags: 'Número de hashtags',
    numberUrls: 'Número de urls',
    interval: 'Intervalo',
    timely: {
      hour: 'A cada hora',
      day: 'Diario',
      week: 'Semanal',
      month: 'Mensual'
    },
    limit: 'Límite',
    button: {
      timeline: 'Ver resultados de la línea de tiempo',
      hashtags: 'Ver resultados del detector de hashtag',
      urls: 'Ver gráfico de URL',
      activity: 'Ver actividad'
    }
  },
  chart: {
    timelineOn: null,
    hashtagsWith: null,
    linkCountOn: null,
    activityOn: null
  },
  table: {
    date: 'Fecha',
    usage: 'Uso',
    urls: 'URLs',
    urlCount: 'Conteo de URLs',
    user: null, // todo: gender neutral?
    count: 'Conteo',
    hashtags: 'Hashtags',
    hashtagFreq: 'Frecuencia de hashtag',
    viewFor: null,
    view: 'Vista de tabla'
  },

  about: require('./about.js')
}
