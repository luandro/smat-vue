export const TWITTER = 'twitter'
export const REDDIT = 'reddit'
export const GAB = 'gab'
export const FOURCHAN = '4chan'
export const EIGHTKUN = '8kun'

export const PRODUCTION = 'production'
export const ENV = (
  process.env.VUE_APP_ENV_DISPLAY ||
    process.env.NODE_ENV ||
    PRODUCTION
)
