# smat-vue

This is a Vue interface for https://gitlab.com/smat-project/smat-be/-/blob/master/DOCS.md


## Development

```bash
$ npm install
$ npm run serve
```

Compiles and hot-reloads for development.
Go the the address it tells you in the browser you'll see the app!

To change the API the app connects to, you can use the environment variable `SMAT_API`

e.g.

```bash
$ VUE_APP_API=http://127.0.0.1:5000 npm run serve
```

## Deploy

Gitlab CI is set up (see `.gitlab-ci.yml`) to deploy code pushed to branches:
- `staging` => https://smat-project.gitlab.io/smat-vue
- `master` => https://www.smat-app.com


## Scripts

`npm run lint` - run the linter

`npm run build` - compiles and minifies for production

### How to

See [the wiki](https://gitlab.com/smat-project/smat-vue/-/wikis/Wiki-home) for some useful guides on how to develop this app

## NOTES

Special thanks to [Protozoa Co-op](http://protozoa.nz/) for getting us started and showing us the ropes!


Useful links:

- vue configuration : https://cli.vuejs.org/config
- vue-cli: https://cli.vuejs.org/guide/
- vue forms : https://vuejs.org/v2/guide/forms.html
- vue-cli guide : https://appdividend.com/2018/02/09/vue-cli-tutorial-2018-example-scratch/
- social media meta tags
  - https://cards-dev.twitter.com/validator
  - https://developers.facebook.com/tools/debug/
  - https://css-tricks.com/essential-meta-tags-social-media/
